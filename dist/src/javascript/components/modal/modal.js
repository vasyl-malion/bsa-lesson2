"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showModal = void 0;
var domHelper_1 = require("../../helpers/domHelper");
var winner_1 = require("./winner");
function showModal(_a) {
    var title = _a.title, bodyElement = _a.bodyElement, _b = _a.onClose, onClose = _b === void 0 ? function () { } : _b;
    var root = getModalContainer();
    var modal = createModal({ title: title, bodyElement: bodyElement, onClose: onClose });
    root.append(modal);
    root.append(winner_1.showWinnerModal);
}
exports.showModal = showModal;
function getModalContainer() {
    return document.getElementById('root');
}
function createModal(_a) {
    var title = _a.title, bodyElement = _a.bodyElement, onClose = _a.onClose;
    var layer = domHelper_1.createElement({ tagName: 'div', className: 'modal-layer' });
    var modalContainer = domHelper_1.createElement({ tagName: 'div', className: 'modal-root' });
    var header = createHeader(title, onClose);
    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);
    return layer;
}
function createHeader(title, onClose) {
    var headerElement = domHelper_1.createElement({ tagName: 'div', className: 'modal-header' });
    var titleElement = domHelper_1.createElement({ tagName: 'span' });
    var closeButton = domHelper_1.createElement({ tagName: 'div', className: 'close-btn' });
    titleElement.innerText = title;
    closeButton.innerText = '×';
    var close = function () {
        hideModal();
        onClose();
    };
    closeButton.addEventListener('click', close);
    headerElement.append(titleElement, closeButton);
    return headerElement;
}
function hideModal() {
    var modal = document.getElementsByClassName('modal-layer')[0];
    modal === null || modal === void 0 ? void 0 : modal.remove();
}
