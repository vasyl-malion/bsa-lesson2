"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.showWinnerModal = void 0;
var domHelper_1 = require("../../helpers/domHelper");
var fighterPreview_1 = require("../fighterPreview");
var modal_1 = require("./modal");
var app_1 = __importDefault(require("../../app"));
function showWinnerModal(fighter) {
    var title = fighter.name + " is winner";
    var bodyElement = createFighter(fighter);
    modal_1.showModal({
        title: title,
        bodyElement: bodyElement,
        onClose: onClose,
    });
}
exports.showWinnerModal = showWinnerModal;
function createFighter(fighter) {
    var imgElement = fighterPreview_1.createFighterImage(fighter);
    var fighterElement = domHelper_1.createElement({
        tagName: 'div',
        className: 'arena___fighter-winner',
    });
    fighterElement.append(imgElement);
    return fighterElement;
}
function onClose() {
    var arena = document.querySelector('.arena___root');
    if (arena) {
        arena.remove();
    }
    new app_1.default();
}
