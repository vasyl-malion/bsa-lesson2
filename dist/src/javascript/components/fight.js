"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBlockPower = exports.getHitPower = exports.getSuperPower = exports.getDamage = exports.fight = void 0;
var controls_1 = require("../../constants/controls");
function fight(firstFighter, secondFighter) {
    return __awaiter(this, void 0, void 0, function () {
        var firstFighterState, secondFighterState;
        return __generator(this, function (_a) {
            firstFighterState = {
                health: firstFighter.health,
                superAttackAvailable: true,
                blockAvailable: false,
                lineHealth: document.getElementById('left-fighter-indicator')
            };
            secondFighterState = {
                health: secondFighter.health,
                superAttackAvailable: true,
                blockAvailable: false,
                lineHealth: document.getElementById('right-fighter-indicator')
            };
            return [2, new Promise(function (resolve) {
                    if (firstFighterState.superAttackAvailable) {
                        useSuperAttack.apply(void 0, __spreadArrays([firstFighterState, function () {
                                firstFighter.health -= getSuperPower(secondFighter);
                                getFighterHealth(firstFighter, firstFighterState.health, firstFighterState.lineHealth);
                            }], controls_1.controls.PlayerOneCriticalHitCombination));
                    }
                    if (secondFighterState.superAttackAvailable) {
                        useSuperAttack.apply(void 0, __spreadArrays([secondFighterState, function () {
                                secondFighter.health -= getSuperPower(firstFighter);
                                getFighterHealth(secondFighter, secondFighterState.health, secondFighterState.lineHealth);
                            }], controls_1.controls.PlayerTwoCriticalHitCombination));
                    }
                    document.addEventListener('keydown', function (e) {
                        switch (e.code) {
                            case controls_1.controls.PlayerOneBlock:
                                firstFighterState.blockAvailable = true;
                                break;
                            case controls_1.controls.PlayerTwoBlock:
                                secondFighterState.blockAvailable = true;
                                break;
                        }
                    });
                    document.addEventListener('keyup', function (e) {
                        switch (e.code) {
                            case controls_1.controls.PlayerOneBlock:
                                firstFighterState.blockAvailable = false;
                                break;
                            case controls_1.controls.PlayerTwoBlock:
                                secondFighterState.blockAvailable = false;
                                break;
                            case controls_1.controls.PlayerOneAttack:
                                attack(firstFighterState, secondFighterState, firstFighter, secondFighter);
                                break;
                            case controls_1.controls.PlayerTwoAttack:
                                attack(secondFighterState, firstFighterState, secondFighter, firstFighter);
                                break;
                        }
                        if (firstFighter.health <= 0) {
                            resolve(secondFighter);
                        }
                        else if (secondFighter.health <= 0) {
                            resolve(firstFighter);
                        }
                    });
                })];
        });
    });
}
exports.fight = fight;
function attack(attackerState, defenderState, attacker, defender) {
    if (!attackerState.blockAvailable) {
        if (!defenderState.blockAvailable) {
            defender.health -= getDamage(attacker, defender);
        }
        getFighterHealth(defender, defenderState.health, defenderState.lineHealth);
    }
}
function getDamage(attacker, defender) {
    var damage = getHitPower(attacker) - getBlockPower(defender);
    damage = damage >= 0 ? damage : 0;
    return damage;
}
exports.getDamage = getDamage;
function getSuperPower(fighter) {
    var power = fighter.attack * 2;
    return power;
}
exports.getSuperPower = getSuperPower;
function getHitPower(fighter) {
    var power = fighter.attack * (1 + Math.random());
    return power;
}
exports.getHitPower = getHitPower;
function getBlockPower(fighter) {
    var block = fighter.defense * (1 + Math.random());
    return block;
}
exports.getBlockPower = getBlockPower;
function getFighterHealth(fighter, initialFighterHealth, lineHealth) {
    var result = fighter.health / initialFighterHealth * 100;
    if (result > 0) {
        lineHealth.style.width = result + "%";
    }
    else {
        lineHealth.style.width = "0%";
    }
    return result;
}
function useSuperAttack(attackerState, func) {
    var codes = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        codes[_i - 2] = arguments[_i];
    }
    var pressed = new Set();
    document.addEventListener('keydown', function (e) {
        pressed.add(e.code);
        for (var _i = 0, codes_1 = codes; _i < codes_1.length; _i++) {
            var code = codes_1[_i];
            if (!pressed.has(code)) {
                return;
            }
        }
        pressed.clear();
        func();
        setTimeout(function () {
            attackerState.superAttackAvailable = false;
        }, 10000);
    });
    document.addEventListener('keyup', function (e) {
        pressed.delete(e.code);
    });
}
