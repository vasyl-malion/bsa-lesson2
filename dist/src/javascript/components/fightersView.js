"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFighters = void 0;
var domHelper_1 = require("../helpers/domHelper");
var fighterSelector_1 = require("./fighterSelector");
function createFighters(fighters) {
    var selectFighter = fighterSelector_1.createFightersSelector();
    var container = domHelper_1.createElement({ tagName: 'div', className: 'fighters___root' });
    var preview = domHelper_1.createElement({ tagName: 'div', className: 'preview-container___root' });
    var fightersList = domHelper_1.createElement({ tagName: 'div', className: 'fighters___list' });
    var fighterElements = fighters.map(function (fighter) { return createFighter(fighter, selectFighter); });
    fightersList.append.apply(fightersList, fighterElements);
    container.append(preview, fightersList);
    return container;
}
exports.createFighters = createFighters;
function createFighter(fighter, selectFighter) {
    var fighterElement = domHelper_1.createElement({ tagName: 'div', className: 'fighters___fighter' });
    var imageElement = createImage(fighter);
    var onClick = function (event) { return selectFighter(event, fighter._id); };
    fighterElement.append(imageElement);
    fighterElement.addEventListener('click', onClick, false);
    return fighterElement;
}
function createImage(fighter) {
    var source = fighter.source, name = fighter.name;
    var attributes = {
        src: source,
        title: name,
        alt: name,
    };
    var imgElement = domHelper_1.createElement({
        tagName: 'img',
        className: 'fighter___fighter-image',
        attributes: attributes
    });
    return imgElement;
}
