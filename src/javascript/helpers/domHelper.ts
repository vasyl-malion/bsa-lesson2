import {ICreateElement} from "../interfaces/interfaces";

export function createElement({ tagName, className, attributes = {} }:ICreateElement):HTMLElement {
  const element:HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames: string[] = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, (<any>attributes)[key]));

  return element;
}
