import { fightersDetails, fighters } from './mockData';

type getFighter = object | undefined;

const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

async function callApi(endpoint: string, method: string): Promise<object>{
  const url: string = API_URL + endpoint;
  const options: object = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
      .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
      .then((result) => JSON.parse(atob(result.content)))
      .catch((error) => {
        throw error;
      });
}

async function fakeCallApi(endpoint: string): Promise<object> {
  const response: getFighter = 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve: any, reject: any): void => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): getFighter {
  const start: number = endpoint.lastIndexOf('/');
  const end: number = endpoint.lastIndexOf('.json');
  const id: string = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id);
}

export { callApi };
