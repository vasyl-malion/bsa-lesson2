interface ICreateElement {
    tagName: string,
    className?: string,
    attributes?: object
}

interface IFighter {
    _id: string,
    name: string,
    source: string,
}

interface IFighterDetails {
    _id: string,
    name: string,
    health: number,
    attack: number,
    defense: number,
    source: string,
}

interface IFighterState {
    health: number,
    superAttackAvailable: boolean,
    blockAvailable: boolean,
    lineHealth: HTMLElement,
}

interface IAttributeImg {
    src: string,
    title: string,
    alt: string
}
interface IControls {
    PlayerOneAttack: string,
    PlayerOneBlock: string,
    PlayerTwoAttack: string,
    PlayerTwoBlock: string,
    PlayerOneCriticalHitCombination: string[],
    PlayerTwoCriticalHitCombination: string[]
}

interface IShowModal {
    title: string,
    bodyElement: HTMLElement,
    onClose: () => void
}

export {
    ICreateElement,
    IFighter,
    IFighterDetails,
    IFighterState,
    IAttributeImg,
    IControls,
    IShowModal
}