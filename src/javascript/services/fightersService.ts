import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string) {

    const endpoint: string = `details/fighter/${id}.json`;

    return await callApi(endpoint, 'GET');
  }
}

export const fighterService = new FighterService();
