import { createElement } from '../helpers/domHelper';

import {IFighter, IAttributeImg, ICreateElement, IFighterState, IFighterDetails} from "../interfaces/interfaces";

export function createFighterPreview(fighter: IFighter, position: string): HTMLElement {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    fighterElement.append(createFighterImage(fighter), fighterInfo(fighter));
  }

  return fighterElement;
}

export function createFighterImage(fighter: IFighter): HTMLElement {
  const { source, name } = fighter;
  const attributes: IAttributeImg = {
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement: HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function oneFighterInfoFeature(key: string, value: number):HTMLElement {

  const feature: HTMLElement = createElement({
    tagName: "div",
    className: "fighter-preview__feature"
  });

  const keyOfFeature: HTMLElement = createElement({tagName: "span"});
  keyOfFeature.innerHTML = key;
  const valueOfFeature: HTMLElement = createElement({
    tagName: "span",
    className: "fighter-preview__feature-value"
  });
  valueOfFeature.innerHTML = String(value) ;

  feature.append(keyOfFeature, valueOfFeature);

  return feature
}

function fighterInfo(fighter: any):HTMLElement {
  const container:HTMLElement = createElement({
    tagName: "div",
    className: "fighter-preview__info-container"
  });

  const name:HTMLElement = createElement({
    tagName: "span",
    className: "fighter-preview__info-name"
  });

  const health:HTMLElement = oneFighterInfoFeature("Health: ", fighter.health);
  const attack:HTMLElement = oneFighterInfoFeature("Attack: ", fighter.attack);
  const defense:HTMLElement = oneFighterInfoFeature("Defense: ", fighter.defense);

  name.innerHTML = fighter.name.toUpperCase();
  container.append(name, health, attack, defense);

  return container;
}