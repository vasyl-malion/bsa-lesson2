import { controls } from '../../constants/controls';
import {IFighterState, IFighterDetails} from "../interfaces/interfaces";

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): Promise<object> {

  const firstFighterState: IFighterState = {
    health: firstFighter.health,
    superAttackAvailable: true,
    blockAvailable: false,
    lineHealth: document.getElementById('left-fighter-indicator') as HTMLElement
  }

  const secondFighterState: IFighterState = {
    health: secondFighter.health,
    superAttackAvailable: true,
    blockAvailable: false,
    lineHealth: document.getElementById('right-fighter-indicator') as HTMLElement
  }

  return new Promise((resolve) => {

    if (firstFighterState.superAttackAvailable) {
      useSuperAttack(firstFighterState, () => {
        firstFighter.health -= getSuperPower(secondFighter);
        getFighterHealth(firstFighter, firstFighterState.health, firstFighterState.lineHealth);
      }, ...controls.PlayerOneCriticalHitCombination);
    }

    if (secondFighterState.superAttackAvailable) {
      useSuperAttack(secondFighterState, () => {
        secondFighter.health -= getSuperPower(firstFighter);
        getFighterHealth(secondFighter, secondFighterState.health, secondFighterState.lineHealth);
      }, ...controls.PlayerTwoCriticalHitCombination);
    }

    document.addEventListener('keydown', (e) => {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighterState.blockAvailable = true;
          break;
        case controls.PlayerTwoBlock:
          secondFighterState.blockAvailable = true;
          break;
      }
    });

    document.addEventListener('keyup', (e) => {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighterState.blockAvailable = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighterState.blockAvailable = false;
          break;
        case controls.PlayerOneAttack:
          attack(firstFighterState, secondFighterState, firstFighter, secondFighter);
          break;
        case controls.PlayerTwoAttack:
          attack(secondFighterState, firstFighterState, secondFighter, firstFighter);
          break;
      }

      if (firstFighter.health <= 0 ) {
        resolve(secondFighter)
      } else if (secondFighter.health <= 0) {
        resolve(firstFighter)
      }
    });
  });
}

/* */
function attack(attackerState: IFighterState, defenderState: IFighterState,
                attacker: IFighterDetails, defender: IFighterDetails): void  {
  if (!attackerState.blockAvailable) {

    if(!defenderState.blockAvailable) {
      defender.health -= getDamage(attacker, defender);
    }

    getFighterHealth(defender, defenderState.health, defenderState.lineHealth);
  }
}

export function getDamage(attacker: IFighterDetails, defender: IFighterDetails): number {
  let damage: number = getHitPower(attacker) - getBlockPower(defender);
  damage = damage >= 0 ? damage : 0;
  return damage
}

export function getSuperPower(fighter: IFighterDetails): number {
  let power: number = fighter.attack * 2;
  return power;
}

export function getHitPower(fighter: IFighterDetails): number {
  const power: number = fighter.attack * (1 + Math.random());
  return power;
}

export function getBlockPower(fighter: IFighterDetails): number {
  const block: number =  fighter.defense * (1 + Math.random());
  return block;
}

function getFighterHealth(fighter: IFighterDetails, initialFighterHealth: number, lineHealth: HTMLElement): number {
  const result: number = fighter.health / initialFighterHealth * 100;

  if (result > 0) {
    lineHealth.style.width = `${result}%`;
  } else {
    lineHealth.style.width = `0%`;
  }
  return result;
}
/* */
function useSuperAttack(attackerState: IFighterState, func, ...codes: string[]): void {
  let pressed = new Set();

  document.addEventListener('keydown', (e) => {
    pressed.add(e.code);
    for (let code of codes) {
      if (!pressed.has(code)) {
        return;
      }
    }
    pressed.clear();
    func()
    setTimeout(() => {
      attackerState.superAttackAvailable = false
    }, 10000);
  });

  document.addEventListener('keyup', (e) => {
    pressed.delete(e.code);
  });
}