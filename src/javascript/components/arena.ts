import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from "./fight";
import {showWinnerModal} from './modal/winner';
import {ICreateElement, IFighterDetails} from '../interfaces/interfaces';

export function renderArena(selectedFighters) {
  const root: HTMLElement = document.getElementById('root') as HTMLElement;
  const arena: HTMLElement = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  // @ts-ignore
  fight(...selectedFighters)
    .then(data => {
      // @ts-ignore
      showWinnerModal(data);

    })
    .catch(err => console.log(err));

}

function createArena(selectedFighters): HTMLElement {
  const arena: HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
  // @ts-ignore
  const healthIndicators = createHealthIndicators(...selectedFighters);
  // @ts-ignore
  const fighters = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators: HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign: HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: any, position: string):HTMLElement {
  const { name } = fighter;
  const container: HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName: HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter, secondFighter): HTMLElement {
  const battleField: HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement: HTMLElement = createFighter(firstFighter, 'left');
  const secondFighterElement: HTMLElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: any, position: string): HTMLElement {
  const imgElement: HTMLElement = createFighterImage(fighter);
  const positionClassName:string = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}