import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';
import App from '../../app';
import {IFighter} from "../../interfaces/interfaces";

export function showWinnerModal(fighter : IFighter): void {
  const title: string = `${fighter.name} is winner`;
  const bodyElement : HTMLElement= createFighter(fighter);
  showModal({
    title,
    bodyElement,
    onClose,
  });
}

function createFighter(fighter: IFighter):HTMLElement {
  const imgElement: HTMLElement = createFighterImage(fighter);
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___fighter-winner',
  });

  fighterElement.append(imgElement);
  return fighterElement;
}

function onClose(): void {
  const arena: HTMLElement | null = document.querySelector('.arena___root');
  if (arena) {
    arena.remove();
  }
  new App();
}