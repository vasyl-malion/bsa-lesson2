import { createElement } from '../../helpers/domHelper';
import {showWinnerModal} from './winner';
import {IShowModal} from "../../interfaces/interfaces";

export function showModal({ title, bodyElement, onClose = () => {} }: IShowModal): void {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });

  root.append(modal);
  // @ts-ignore
  root.append(showWinnerModal)
}

function getModalContainer(): HTMLElement {
  return document.getElementById('root') as HTMLElement;
}

function createModal({ title, bodyElement, onClose }: IShowModal) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: () => void): HTMLElement {
  const headerElement: HTMLElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement: HTMLElement = createElement({ tagName: 'span' });
  const closeButton: HTMLElement = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = (): void => {
    hideModal();
    onClose();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal(): void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
